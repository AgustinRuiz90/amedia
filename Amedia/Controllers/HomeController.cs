﻿using Amedia.Models;
using Amedia.Repositories.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Amedia.Controllers
{
    public class HomeController : Controller
    {
        public UsuarioRepository UsuarioRepository { get; set; }
        public HomeController()
        {
            this.UsuarioRepository = new UsuarioRepository();
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(LoginDTO login)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = UsuarioRepository.GetByCredentials(login.UserName.ToLower(), login.Clave);
                    Session["UserID"] = user.Id;
                    //Un enum de Roles sería mejor pero sólo tengo dos a modo de ejemplo
                    Session["IdRol"] = user.IdRol;
                    if (user.IdRol == 1)
                    {
                        return RedirectToAction("Index", "Visitante");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Usuario");
                    }
                } else
                {
                    return View();
                }
                
            }
            catch (KeyNotFoundException kex)
            {
                ModelState.AddModelError("Clave", "Usuario y/o contraseña inválida");
                return View();
            }
        }
    }
}