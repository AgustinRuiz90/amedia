﻿using Amedia.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Amedia.Repositories
{
    public interface IRepository<T> where T : Entity<T>
    {
        IList<T> GetAll();
        T GetById(int id);
        void Delete(int id);
        void Insert(T entity);
        void Update(T entity);
    }
}