﻿using Amedia.Helpers;
using Amedia.Models;
using Amedia.Repositories;
using Amedia.Repositories.Implementations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Amedia.Controllers
{
    [SessionCheck]
    [AdminOnly]
    public class UsuarioController : Controller
    {
        public UsuarioRepository UsuarioRepository { get; set; }
        public IRepository<Rol> RolRepository { get; set; }
        public UsuarioController()
        {
            this.UsuarioRepository = new UsuarioRepository();
            this.RolRepository = new Repository<Rol>();
        }
        // GET: Usuario
        public ActionResult Index()
        {
            var list = UsuarioRepository.GetAll().Where(x => x.Activo);
            return View(list);
        }

        // GET: Usuario/Details/5
        public ActionResult Details(int id)
        {
            var usuario = UsuarioRepository.GetById(id);
            return View(usuario);
        }

        // GET: Usuario/Create
        public ActionResult Create()
        {
            ViewBag.Roles = RolRepository.GetAll();
            return View();
        }

        // POST: Usuario/Create
        [HttpPost]
        public ActionResult Create(Usuario usuario)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    usuario.Clave = PasswordHelper.EncodePassword(usuario.Clave);
                    usuario.Rol = RolRepository.GetById(usuario.Rol.Id);
                    UsuarioRepository.Insert(usuario);

                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Roles = RolRepository.GetAll();
                    return View();
                }
            }
            catch
            {
                ViewBag.Roles = RolRepository.GetAll();
                return View();
            }
        }

        // GET: Usuario/Edit/5
        public ActionResult Edit(int id)
        {
            ViewBag.Roles = RolRepository.GetAll();
            var usuario = UsuarioRepository.GetById(id);
            return View(usuario);
        }

        // POST: Usuario/Edit/5
        [HttpPost]
        public ActionResult Edit(Usuario usuario)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    usuario.Rol = RolRepository.GetById(usuario.Rol.Id);
                    UsuarioRepository.Update(usuario);
                    return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.Roles = RolRepository.GetAll();
                    return View();
                }

            }
            catch
            {
                ViewBag.Roles = RolRepository.GetAll();
                return View();
            }
        }

        // GET: Usuario/Delete/5
        public ActionResult Delete(int id)
        {
            var usuario = UsuarioRepository.GetById(id);
            return View(usuario);
        }

        // POST: Usuario/Delete/5
        [HttpPost]
        public ActionResult Delete(Usuario usuario)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //Baja lógica
                    usuario.Activo = false;
                    UsuarioRepository.Update(usuario);
                    return RedirectToAction("Index");
                }
                else
                {
                    return View();
                }

            }
            catch
            {
                return View();
            }
        }
    }
}
