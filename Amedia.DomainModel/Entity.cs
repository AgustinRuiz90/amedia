﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace Amedia.Models
{
    public abstract class Entity<T> where T : class
    {
        public virtual int Id { get; set; }
        public abstract string GetTableName();
        public abstract T Map(DbDataReader data);
    }
}