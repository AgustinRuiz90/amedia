﻿using Amedia.DomainModel;
using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Common;

namespace Amedia.Models
{
    public class Usuario : Entity<Usuario>
    {
        [Required]
        [StringLength(100, ErrorMessage = "El {0} debe tener al menos {2} caracteres.", MinimumLength = 6)]
        public string Username { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "El {0} debe tener al menos {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Clave { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public bool Activo { get; set; }
        public int IdRol { get; set; }
        private Rol _rol { get; set; }
        public Rol Rol {
            get
            {
                return _rol;
            }
            set
            {
                IdRol = ((Rol)value).Id;
                _rol = value;
            }
        }

public override string GetTableName()
        {
            return "Usuario";
        }

        public override Usuario Map(DbDataReader data)
        {
            return new Usuario
            {
                Id = data.GetReaderField<int>("Id"),
                Username = data.GetReaderField<string>("Username"),
                Clave = data.GetReaderField<string>("Clave"),
                Activo = data.GetReaderField<bool>("Activo"),
                Email = data.GetReaderField<string>("Email"),
                Nombre = data.GetReaderField<string>("Nombre"),
                Rol = new Rol
                {
                    Id = data.GetReaderField<int>("IdRol"),
                    Descripcion = data.GetReaderField<string>("Rol_Descripcion"),
                }
            };
        }
    }
}