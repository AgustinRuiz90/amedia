﻿using Amedia.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Amedia.Repositories
{
    public class Repository<T> : IRepository<T> where T : Entity<T>
    {
        public DbService DbService { get; set; }
        private T Accessor => (T)Activator.CreateInstance(typeof(T));

        public Repository()
        {
            DbService = new DbService();
        }

        public virtual IList<T> GetAll()
        {
            var query = "SELECT * FROM " + Accessor.GetTableName();
            var command = new SqlCommand(query);
            var ret = new List<T>();
            DbService.ExecuteCommand(command, reader => ret.Add(Accessor.Map(reader)));

            return ret;
        }

        public virtual T GetById(int id)
        {
            var query = "SELECT * FROM " + Accessor.GetTableName() + " WHERE Id = " + id;
            var command = new SqlCommand(query);
            T ret = (T)Activator.CreateInstance(typeof(T));
            DbService.ExecuteCommand(command, reader => ret = ret.Map(reader));

            return ret;
        }

        public void Delete(int id)
        {
            var query = "DELETE FROM " + Accessor.GetTableName() + " WHERE Id = " + id;
            var command = new SqlCommand(query);
            DbService.ExecuteNonQuery(command);
        }

        public void Insert(T entity)
        {
            var query = "INSERT INTO " + entity.GetTableName() + " (";
            foreach (var property in entity.GetType().GetProperties().Where(x => x.Name != "Id"))
            {
                if (!property.PropertyType.Namespace.StartsWith("System")) continue;
                query += property.Name + ", ";
            }

            query = query.Substring(0, query.Length - 2);

            query += ") VALUES (";

            foreach (var property in entity.GetType().GetProperties().Where(x => x.Name != "Id"))
            {
                if (!property.PropertyType.Namespace.StartsWith("System")) continue;
                if (IsNumericType(property.PropertyType))
                    query += entity.GetType().GetProperty(property.Name).GetValue(entity) + ", ";
                else
                    query += "'" + entity.GetType().GetProperty(property.Name).GetValue(entity) + "', ";
            }

            query = query.Substring(0, query.Length - 2);
            query += ")";

            var command = new SqlCommand(query);
            DbService.ExecuteNonQuery(command);
        }

        public void Update(T entity)
        {
            var query = "UPDATE " + entity.GetTableName() + " SET ";
            foreach (var property in entity.GetType().GetProperties().Where(x => x.Name != "Id"))
            {
                if (!property.PropertyType.Namespace.StartsWith("System")) continue;
                query += property.Name + " = ";
                if (IsNumericType(property.PropertyType))
                    query += entity.GetType().GetProperty(property.Name).GetValue(entity) + ", ";
                else
                    query += "'" + entity.GetType().GetProperty(property.Name).GetValue(entity) + "', ";
            }

            query = query.Substring(0, query.Length - 2);

            query += " WHERE Id = " + entity.Id;

            var command = new SqlCommand(query);
            DbService.ExecuteNonQuery(command);
        }

        public bool IsNumericType(Type type)
        {
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    return true;
                default:
                    return false;
            }
        }
    }
}