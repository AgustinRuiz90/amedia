﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Amedia.Repositories
{
    public class DbService
    {
        private string connectionString => ConfigurationManager.ConnectionStrings["AmediaDb"].ConnectionString;

        public int ExecuteNonQuery(DbCommand command)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                command.Connection = conn;

                return command.ExecuteNonQuery();
            }
        }

        public void ExecuteCommand(DbCommand command, Action<DbDataReader> action)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                command.Connection = conn;
                command.CommandTimeout = 60;
                conn.Open();

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        action(reader);
                    }

                    reader.Close();
                }
            }
        }

    }
}