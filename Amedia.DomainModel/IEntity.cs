﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace Amedia.Models
{
    public interface IEntity<T>
    {
        string GetTableName();
        T Map(DbDataReader data);
    }
}