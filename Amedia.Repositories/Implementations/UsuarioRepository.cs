﻿using Amedia.Helpers;
using Amedia.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amedia.Repositories.Implementations
{
    public class UsuarioRepository : Repository<Usuario>
    {
        public override IList<Usuario> GetAll()
        {
            var query = "SELECT U.*, R.Descripcion as Rol_Descripcion FROM Usuario U INNER JOIN Rol R ON U.IdRol = R.Id";
            var command = new SqlCommand(query);
            var ret = new List<Usuario>();
            DbService.ExecuteCommand(command, reader => ret.Add(new Usuario().Map(reader)));

            return ret;

        }

        public Usuario GetByCredentials(string usuario, string clave)
        {
            var query = "SELECT U.*, R.Descripcion as Rol_Descripcion FROM Usuario U INNER JOIN Rol R ON U.IdRol = R.Id AND LOWER(U.Username) = '" + usuario + "'";
            var command = new SqlCommand(query);
            var ret = new Usuario();
            DbService.ExecuteCommand(command, reader => ret = new Usuario().Map(reader));

            if (ret.Id != 0)
            {
                if (PasswordHelper.VerifyPassword(ret.Clave, clave))
                    return ret;
                else
                    throw new KeyNotFoundException();
            }
            else
            {
                throw new KeyNotFoundException();
            }
        }

        public override Usuario GetById(int id)
        {
            var query = "SELECT U.*, R.Descripcion as Rol_Descripcion FROM Usuario U INNER JOIN Rol R ON U.IdRol = R.Id AND U.Id = " + id;
            var command = new SqlCommand(query);
            var ret = new Usuario();
            DbService.ExecuteCommand(command, reader => ret = new Usuario().Map(reader));

            return ret;

        }
    }
}
