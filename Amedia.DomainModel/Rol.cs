﻿using Amedia.DomainModel;
using System.Data.Common;

namespace Amedia.Models
{
    public class Rol : Entity<Rol>
    {
        public string Descripcion { get; set; }

        public override string GetTableName()
        {
            return "Rol";
        }

        public override Rol Map(DbDataReader data)
        {
            return new Rol
            {
                Id = data.GetReaderField<int>("Id"),
                Descripcion = data.GetReaderField<string>("Descripcion")
            };
        }
    }
}