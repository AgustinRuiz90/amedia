﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amedia.DomainModel
{
    public static class MaxExtensions
    {
        public static T GetReaderField<T>(this DbDataReader reader, string fieldName)
        {
            var ordinal = reader.GetOrdinal(fieldName);

            if (reader.IsDBNull(ordinal)) return default(T);

            return reader.GetFieldValue<T>(ordinal);
        }
    }
}
